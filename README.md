<!-- NAME -->

## Name

SymOAuth

<!-- DESCRIPTION -->

## Description

This project is an application allowing the connection of a user via his Github account (OAuth / SSO). This project uses the [Github API](https://docs.github.com/en/rest). The application is based on the [Symfony 6.0](https://symfony.com/doc/6.0/index.html) Framework.

<!-- CONTEXT -->

## Context

This project is a school project, which refers to the "Learning Lab" subject of MyDigitalSchool in MBA 2 Full-Stack Developer.

The goal of this course was to discover a technology chosen from a non-exhaustive list, containing for example: Docker, SSO, OAuth, RabbitMQ, Redis, etc... In terms of reporting, a report in PDF format and an oral presentation are expected.

The creation of one or more prototypes was also requested. This project concerns the second prototype. To see the prototype 1, click [here](https://github.com/Projets-Formation-DorianP/mds-oauth-google).

The second prototype was to establish the connection without external dependencies, in order to see the difference with the implementation of dependencies specialized in OAuth connections for SYmfony, and in particular the bundle, [KnpUniversity - OAuth2 CLient Bundle](https://github.com/knpuniversity/oauth2-client-bundle).

<!-- INSTALLATION -->

## Installation

This project requires Docker and Docker Compose to be used.

The first thing to do is to launch the containers using Docker Compose, with the following command:

    docker-compose up -d

Once the containers are created, we will have to install the dependencies with the help of composer, for that:

    docker-compose exec www bash
    cd project/
    composer install

Once the dependencies are installed, we can stay within the container at the same place, and take care of the database:

    php bin/console d:d:c --if-exists
    php bin/console d:m:m

Once the database is created, you just have to go to the address [http://127.0.0.1:8741/](http://127.0.0.1:8741/)

<!-- CONFGURATION -->

## Configuration

At the configuration level, you will have to create an .env.dev with the same information as the .env, and fill in the environment variables: `GITHUB_ID` & `GITHUB_SECRET`

You can find these two informations by creating a new OAuth application via the link : https://github.com/settings/applications/new

<!-- USAGE -->

## Usage

Once you have finished the Installation & Configuration paragraphs, you just have to go to the address [http://127.0.0.1:8741/](http://127.0.0.1:8741/)

<!-- PROJECT STATUS -->

## Project Status

This project is considered finished. It is awaiting grading.
