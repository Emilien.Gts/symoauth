<?php

namespace App\Security;

use App\Entity\Profile\GithubProfile;
use App\Entity\User;
use App\Provider\GithubUserProvider;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\SelfValidatingPassport;

class GithubAuthenticator extends AbstractAuthenticator
{
    private GithubUserProvider $provider;

    private EntityManagerInterface $manager;

    public function __construct(GithubUserProvider $provider, EntityManagerInterface $manager)
    {
        $this->provider = $provider;
        $this->manager = $manager;
    }

    public function supports(Request $request): ?bool
    {
        return $request->query->get('code') ?? false;
    }

    public function authenticate(Request $request): Passport
    {
        $data = $this->provider->loadUserData(
            $request->query->get('code')
        );

        return new SelfValidatingPassport(
            new UserBadge($data['profile']['id'], function ($githubId) use ($data) {
                /** @var UserRepository */
                $userRepository = $this->manager->getRepository(User::class);
                $user = $userRepository->findByGithubId($githubId);

                $user = !$user ? new User($data) : User::update($user[0], $data);

                $this->manager->persist($user);
                $this->manager->flush();

                return $user;
            })
        );
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): ?Response
    {
        return null;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): ?Response
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    public function start(Request $request, AuthenticationException $authException = null): Response
    {
        return new JsonResponse('Unauthorized.');
    }
}
