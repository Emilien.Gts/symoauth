<?php

namespace App\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class SecurityController extends AbstractController
{
    private string $githubClientId;

    public function __construct()
    {
        $this->githubClientId = $_ENV['GITHUB_CLIENT_ID'];
    }

    #[Route('/login', name: 'security.login')]
    public function login(): Response
    {
        if($this->getUser()) {
            return new RedirectResponse('/dashboard');
        }

        return $this->render('security/login.html.twig');
    }

    #[Route('/login/github', name: 'security.login.github')]
    public function loginGithub(UrlGeneratorInterface $urlGeneratorInterface)
    {
        $url = $urlGeneratorInterface->generate('dashboard.index', [], UrlGeneratorInterface::ABSOLUTE_URL);
        return new RedirectResponse('https://github.com/login/oauth/authorize?client_id=' . $this->githubClientId . '&redirect_uri=' . $url);
    }
}
