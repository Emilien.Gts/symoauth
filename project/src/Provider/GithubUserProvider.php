<?php

namespace App\Provider;

use Symfony\Contracts\HttpClient\HttpClientInterface;

class GithubUserProvider
{
    private string $githubClientId;

    private string $githubSecret;

    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->githubClientId = $_ENV['GITHUB_CLIENT_ID'];
        $this->githubSecret = $_ENV['GITHUB_SECRET'];
        $this->httpClient = $httpClient;
    }

    public function loadUserData(string $code)
    {
        $url = sprintf(
            'https://github.com/login/oauth/access_token?client_id=%s&client_secret=%s&code=%s',
            $this->githubClientId,
            $this->githubSecret,
            $code
        );

        $response = $this->httpClient->request('POST', $url, [
            'headers' => [
                'Accept' => 'application/json'
            ]
        ]);

        $token = $response->toArray()['access_token'];

        $response = $this->httpClient->request('GET', 'https://api.github.com/user', [
            'headers' => [
                'Authorization' => 'token ' . $token
            ]
        ]);

        $profileData = $response->toArray();

        $response = $this->httpClient->request('GET', 'https://api.github.com/user/public_emails', [
            'headers' => [
                'Authorization' => 'token ' . $token
            ]
        ]);

        $email = $this->getGoodEmail($response->toArray());

        return [
            'profile' => $profileData,
            'email' => $email
        ];
    }

    private function getGoodEmail(array $emails): string|\Exception
    {
        foreach ($emails as $email) {
            if ($email['verified'] === true && $email['primary'] === true) {
                return $email['email'];
            }
        }

        return new \Exception('No verified email was found in the Github account.');
    }
}
